//Funções da Página principal

//card-lobo
let wolves_container = $.querySelector("#card-lobo")

//Promise já tratada em api.js
getWolves()
//Utiliza os dados já tratados
.then(wolves => {
    let min = 1
    let max = wolves.length
    let random1 = Math.floor(Math.random() * (max - min)) + min;
    let random2 = Math.floor(Math.random() * (max - min)) + min;

    while(random1 == random2){
        random2 = Math.floor(Math.random() * (max - min))+ min;
    }
    //Criação dos cards dos lobos
    let card1 = createWolfcard(wolves[random1])
    let card2 = createWolfcard(wolves[random2])
    card1.querySelector("button").remove()
    card2.querySelector("button").remove()
    
    wolves_container.append(card1) 
    wolves_container.append(card2)
//    console.log(card1)


})