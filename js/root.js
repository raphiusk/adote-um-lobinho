//Declarações e funções que outras páginas poderão utilizar
const $ = document


//Manipulação do header das páginas
let header_container = $.querySelector(".header-container")
window.addEventListener("scroll", () => {
    if(window.pageYOffset > 0){
        header_container.classList.add("header-active")
    }else{
        header_container.classList.remove("header-active")
    }
})

//Função criar lobo
const createWolfcard = (wolf) => {
    let wolfCard = $.createElement("div");
    wolfCard.innerHTML = `
            <div class="image-div-back">
                <img class="card-lobo-imagem" src=${wolf.link_image} alt="Imagem de lobo">           
            </div>
            <div class="card-lobo-texto">
                <div>
                    <div>
                        <h3>Nome: ${wolf.name}</h3>
                        <h3 class="age-subtitle">Idade: ${wolf.age} ano${wolf.age > 1? "s":""} </h3>
                    </div>
                    <button class="button-main" onclick="window.location.href = 'wolf.html?id=${wolf.id}' "> Adotar </button>
                </div>
                <p>${wolf.description}</p>
            </div>

    `
    wolfCard.classList.add('card-lobo')
    
    return wolfCard;
}